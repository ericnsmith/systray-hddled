# systray-hddLed

A simple application to display hard disk activity in Linux with an icon in the system tray.

## hddActivity module

The engine for monitoring hard disk activity. It's simply a loop that checks every 10th of a second for changes in the paging to and from disk to memory contained in the /proc/vmstat file.

This module is used for other applications that monitor hard disk usage such 
as the [keyboard-hddLed](https://gitlab.com/ericnsmith/keyboard-hddled) command line tool.

## systray-hddLed module

Creates a system tray icon and then changes the icon when disk activity and activity cessation is detected.

