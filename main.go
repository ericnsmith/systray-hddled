package main

import (
	_ "embed"
	"fmt"
	"github.com/energye/systray"
	"gitlab.com/ericnsmith/systray-hddled/hddActivity"
	"log"
	"os"
	"path/filepath"
	"time"
)

//go:embed images/hard-drive-on.png
var activeIcon []byte

//go:embed images/hard-drive-off.png
var inactiveIcon []byte

func main() {
	if err := setLog(); err != nil {
		log.Fatalln(err)
	}

	// pause for a second to make sure the systray is running
	time.Sleep(time.Second)

	systray.Run(onSystrayReady, shutdown)
}

func onSystrayReady() {
	systray.SetTitle("HDD Activity")
	systray.SetTooltip("HDD Activity")
	systray.SetIcon(inactiveIcon)

	quitMenu := systray.AddMenuItem("Quit", "Quit HDD Activity")
	quitMenu.Click(shutdown)

	if err := hddActivity.Run(hddActive, hddInactive); err != nil {
		log.Fatalln(err)
	}
}

func hddActive() error {
	systray.SetIcon(activeIcon)
	return nil
}

func hddInactive() error {
	systray.SetIcon(inactiveIcon)
	return nil
}

func shutdown() {
	hddActivity.Stop()
	os.Exit(0)
}

func setLog() error {
	userConfigDir, err := os.UserConfigDir()
	if err != nil {
		return fmt.Errorf("failed to retrieve user config directory: %w", err)
	}

	logfileDirPath := filepath.Join(userConfigDir, "systray-hddLed")

	if !fileExists(logfileDirPath) {
		if err := os.MkdirAll(logfileDirPath, os.ModePerm); err != nil {
			return fmt.Errorf("failed to create logfile directory '%s': %w", logfileDirPath, err)
		}
	}

	logfilePath := filepath.Join(logfileDirPath, "systray-hddLed.log")
	logfile, err := os.OpenFile(logfilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to open log file '%s': %w", logfilePath, err)
	}

	log.SetOutput(logfile)

	return nil
}

func fileExists(pathToCheck string) bool {
	_, err := os.Stat(pathToCheck)
	return !os.IsNotExist(err)
}
