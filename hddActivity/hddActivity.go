package hddActivity

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

type HddActivity struct {
}

type vmstatData struct {
	pgpgin  string
	pgpgout string
}

var enabled = false

func Run(onActive, onInactive func() error) error {
	const samplingDelay = 100

	var oldVmstat, newVmstat vmstatData
	var err error
	isActive := false
	enabled = true

	for {
		if !enabled {
			break
		}

		time.Sleep(samplingDelay * time.Millisecond)

		if newVmstat, err = getVmstat(); err != nil {
			return err
		}

		if oldVmstat == newVmstat {
			if isActive {
				if err = onInactive(); err != nil {
					return err
				}

				isActive = false
			}
		} else {
			if !isActive {
				if err = onActive(); err != nil {
					return err
				}

				isActive = true
			}

			oldVmstat = newVmstat
		}
	}

	return nil
}

func Stop() {
	enabled = false
}

func getVmstat() (vmstatData, error) {
	const vmstatFilePath = "/proc/vmstat"

	returnValue := vmstatData{}

	vmstatFile, err := os.Open(vmstatFilePath)
	if err != nil {
		return returnValue, fmt.Errorf("failed to open vmstat file: %w", err)
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(vmstatFile)

	var haveIn, haveOut bool

	scanner := bufio.NewScanner(vmstatFile)
	for scanner.Scan() {
		keyValues := strings.Split(scanner.Text(), " ")
		key := keyValues[0]
		value := keyValues[1]

		if key == "pgpgin" {
			returnValue.pgpgin = value
			haveIn = true
		} else if key == "pgpgout" {
			returnValue.pgpgout = value
			haveOut = true
		}

		if haveIn && haveOut {
			break
		}
	}

	return returnValue, nil
}
